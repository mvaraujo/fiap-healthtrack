CREATE TABLE Usuarios (
    Id               NUMBER NOT NULL,
    NomeCompleto     VARCHAR2(128) NOT NULL,
    DataNascimento   DATE NOT NULL,
    Genero           CHAR(1) NOT NULL,
    Altura           NUMBER NOT NULL,
    EMail            VARCHAR2(64) NOT NULL,
    SenhaHash        VARCHAR2(256) NOT NULL,
    SenhaSalt        NUMBER NOT NULL
);

ALTER TABLE Usuarios ADD CONSTRAINT Usuarios_PK PRIMARY KEY ( Id );

/* ---------- */

CREATE TABLE UsuariosSenhaTokens (
    GUID                RAW(16) NOT NULL,
    UsuarioID           NUMBER NOT NULL,
    DataHoraExpiracao   TIMESTAMP NOT NULL
);

CREATE INDEX UsuariosSenhaTokens__IDX ON
    UsuariosSenhaTokens ( GUID ASC, UsuarioID ASC, DataHoraExpiracao ASC );

ALTER TABLE UsuariosSenhaTokens
    ADD CONSTRAINT SenhaToken_X_Usuario_FK FOREIGN KEY ( UsuarioID )
        REFERENCES Usuarios ( Id );

/* ---------- */

CREATE TABLE UsuarioRegistrosPeso (
    Id                 NUMBER NOT NULL,
    UsuarioId          NUMBER NOT NULL,
    Peso               NUMBER NOT NULL,
    DataHoraRegistro   TIMESTAMP NOT NULL
);

ALTER TABLE UsuarioRegistrosPeso ADD CONSTRAINT UsuarioRegistrosPeso_PK PRIMARY KEY ( Id );

ALTER TABLE UsuarioRegistrosPeso
    ADD CONSTRAINT Peso_X_Usuarios_FK FOREIGN KEY ( UsuarioId )
        REFERENCES Usuarios ( Id );

/* ---------- */

CREATE TABLE UsuarioRegistrosPressao (
    Id                  NUMBER NOT NULL,
    UsuarioID           NUMBER NOT NULL,
    PressaoSistolica    NUMBER NOT NULL,
    PressaoDiastolica   NUMBER NOT NULL,
    DataHoraRegistro    TIMESTAMP NOT NULL
);

ALTER TABLE UsuarioRegistrosPressao ADD CONSTRAINT UsuarioRegistrosPressao_PK PRIMARY KEY ( Id );

ALTER TABLE UsuarioRegistrosPressao
    ADD CONSTRAINT Pressao_X_Usuarios_FK FOREIGN KEY ( UsuarioID )
        REFERENCES Usuarios ( Id );

/* ---------- */

CREATE TABLE ExercicioCategorias (
    Id       NUMBER NOT NULL,
    PaiId    NUMBER,
    Titulo   VARCHAR2(64) NOT NULL
);

ALTER TABLE ExercicioCategorias ADD CONSTRAINT ExercicioCategorias_PK PRIMARY KEY ( Id );

ALTER TABLE ExercicioCategorias
    ADD CONSTRAINT CategoriaExercicio_X_Pai_FK FOREIGN KEY ( PaiId )
        REFERENCES ExercicioCategorias ( Id );

/* ---------- */

CREATE TABLE UsuarioRegistroAtividadeFisica (
    Id                 NUMBER NOT NULL,
    UsuarioId          NUMBER NOT NULL,
    CategoriaId        NUMBER NOT NULL,
    Calorias           NUMBER,
    Anotacoes          CLOB,
    DataHoraRegistro   TIMESTAMP NOT NULL
);

ALTER TABLE UsuarioRegistroAtividadeFisica ADD CONSTRAINT UsuarioAtividadeFisica_PK PRIMARY KEY ( Id );

ALTER TABLE UsuarioRegistroAtividadeFisica
    ADD CONSTRAINT Exercicio_X_Categoria_FK FOREIGN KEY ( CategoriaId )
        REFERENCES ExercicioCategorias ( Id );

ALTER TABLE UsuarioRegistroAtividadeFisica
    ADD CONSTRAINT Exercicio_X_Usuarios_FK FOREIGN KEY ( UsuarioId )
        REFERENCES Usuarios ( Id );

/* ---------- */

CREATE TABLE UsuarioRegistroAlimentacao (
    Id                 NUMBER NOT NULL,
    UsuarioId          NUMBER NOT NULL,
    Calorias           NUMBER NOT NULL,
    Descricao          CLOB,
    DataHoraRegistro   TIMESTAMP
);

ALTER TABLE UsuarioRegistroAlimentacao ADD CONSTRAINT UsuarioRegistroAlimentacao_PK PRIMARY KEY ( Id );

ALTER TABLE UsuarioRegistroAlimentacao
    ADD CONSTRAINT Alimentacao_X_Usuario_FK FOREIGN KEY ( UsuarioId )
        REFERENCES Usuarios ( Id );