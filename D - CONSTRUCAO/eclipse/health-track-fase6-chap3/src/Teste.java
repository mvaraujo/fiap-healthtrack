import br.com.fiap.health_track.Class.*;
import br.com.fiap.health_track.Class.dao.*;

public class Teste {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		RegistroAlimentacaoDAO registroAlimentacaoDAO = new RegistroAlimentacaoDAO();
		
		for(RegistroAlimentacao registroAlimentacao : registroAlimentacaoDAO.getAll())
		{
			System.out.println(
					String.format(
							"%d calorias registradas com descrição \"%s\"",
							registroAlimentacao.getCalorias(),
							registroAlimentacao.getDescricao()
						)
				);		
		}
	}
}