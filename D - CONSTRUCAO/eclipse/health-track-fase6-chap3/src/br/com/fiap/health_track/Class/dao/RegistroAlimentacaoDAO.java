package br.com.fiap.health_track.Class.dao;

import java.util.*;
import br.com.fiap.health_track.Class.*;

/**
 * Registro de alimentação DAO
 */
public class RegistroAlimentacaoDAO {
	public ArrayList<RegistroAlimentacao> getAll()
	{
		ArrayList<RegistroAlimentacao> list = new ArrayList<RegistroAlimentacao>();
		
		list.add(new RegistroAlimentacao(1400, "Primeira medição"));
		list.add(new RegistroAlimentacao(1200, "Segunda medição"));
		list.add(new RegistroAlimentacao(400, "Terceira medição"));
		list.add(new RegistroAlimentacao(1800, "Quarta medição"));
		list.add(new RegistroAlimentacao(200, "Quinta medição"));
		list.add(new RegistroAlimentacao(3200, "Sexta medição"));
		list.add(new RegistroAlimentacao(2200, "Sétima medição"));
		list.add(new RegistroAlimentacao(1200, "Oitava medição"));
		list.add(new RegistroAlimentacao(1800, "Nona medição"));
		list.add(new RegistroAlimentacao(200, "Décima medição"));
		
		return list;		
	};	
}