package br.com.fiap.health_track.Class;

/**
 * Registro de alimentação
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class RegistroAlimentacao extends RegistroMedicaoComentada {
	private int calorias;

	/**
	 * @param calorias
	 * @param descricao
	 */
	public RegistroAlimentacao(int calorias, String descricao) {
		super(descricao);
		this.calorias = calorias;
	}

	/**
	 * Calorias ingeridas
	 */
	public int getCalorias() {
		return calorias;
	}
}