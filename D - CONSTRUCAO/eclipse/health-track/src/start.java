import java.util.Date;

import br.com.fiap.health_track.Class.*;

public class start {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		Usuario usu = new Usuario("Marcus Vinicius Pompeu", Genero.MASCULINO, new Date(1972, 01, 29), (double)1.82);
		
		try {
			System.out.println(
					String.format("%s (%f)", usu.imcDescricao(), usu.imcValor())
				);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		usu.registraPeso(95.0);
		
		System.out.println(
				String.format("%s (%f)", usu.imcDescricao(), usu.imcValor())
			);
	}
}