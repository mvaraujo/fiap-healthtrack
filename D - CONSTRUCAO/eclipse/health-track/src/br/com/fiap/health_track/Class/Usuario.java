package br.com.fiap.health_track.Class;

import java.util.Date;
import java.util.ArrayList;

/**
 * Usuário do sistema, com seus dados pessoais e seu histórico
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class Usuario {
	private String nomeCompleto;
	private Date dataNascimento;
	private Genero genero;
	private double altura;

	private String eMail;
	private String senhaHash;
	private int senhaSalt;

	private ArrayList<RegistroPeso> registrosPeso;
	private ArrayList<RegistroPressao> registrosPressao;
	private ArrayList<RegistroAtividadeFisica> registrosAtividadeFisica;
	private ArrayList<RegistroAlimentacao> registrosAlimentacao;

	/**
	 * Construtor privado para garantir inicialização dos arrays
	 */
	private Usuario() {
		super();
		this.registrosPeso = new ArrayList<RegistroPeso>();
		this.registrosPressao = new ArrayList<RegistroPressao>();
		this.registrosAtividadeFisica = new ArrayList<RegistroAtividadeFisica>();
		this.registrosAlimentacao = new ArrayList<RegistroAlimentacao>();
	}

	/**
	 * @param nomeCompleto
	 * @param altura
	 */
	public Usuario(String nomeCompleto, Genero genero, Date dataNascimento, double altura) {
		this();
		this.nomeCompleto = nomeCompleto;
		this.genero = genero;
		this.dataNascimento = dataNascimento;
		this.altura = altura;
	}

	/**
	 * Nome completo
	 */
	public String getNomeCompleto() {
		return nomeCompleto;
	}

	/**
	 * Data de nascimento
	 */
	public Date getDataNascimento() {
		return dataNascimento;
	}

	/**
	 * Classificação de gênero
	 */
	public Genero getGenero() {
		return genero;
	}

	/**
	 * Altura em metros
	 */
	public double getAltura() {
		return altura;
	}

	/**
	 * Endereço de e-mail
	 */
	public String getEMail() {
		return eMail;
	}

	/**
	 * Hash da senha de acesso
	 */
	public String getSenhaHash() {
		return senhaHash;
	}

	/**
	 * Salt do hash da senha de acesso
	 */
	public int getSenhaSalt() {
		return senhaSalt;
	}

	/**
	 * Registra medição de peso
	 */
	public void registraPeso(double peso) {
		this.registrosPeso.add(new RegistroPeso(peso));
	}

	/**
	 * Registra medição de pressão
	 */
	public void registraPressao(int pressaoSistolica, int pressaoDiastolica) {
		this.registrosPressao.add(new RegistroPressao(pressaoSistolica, pressaoDiastolica)

		);
	}

	/**
	 * Registra prática de atividade física
	 */
	public void registraAtividadeFisica(ExercicioCategoria exercicioCategoria, int calorias, String anotacoes) {
		this.registrosAtividadeFisica.add(new RegistroAtividadeFisica(exercicioCategoria, calorias, anotacoes));
	}

	/**
	 * Registra alimentação
	 */
	public void registraAlimentacao(int calorias, String descricao) {
		this.registrosAlimentacao.add(new RegistroAlimentacao(calorias, descricao));
	}

	/**
	 * Calcula o valor do IMC de acordo com a última medição de pesos
	 * 
	 * @throws Exception caso não haja medições de peso
	 */
	public double imcValor() throws Exception {
		if (registrosPeso.isEmpty()) {
			throw new Exception("[imcValor] Não há pesos registrados!");
		}

		return
				registrosPeso.get(registrosPeso.size() - 1).getPeso() /
				Math.pow(this.altura, 2);
	}
	
	/**
	 * Retorna a descrição para imcValor()
	 */
	public String imcDescricao() throws Exception
	{
		double imc = this.imcValor();
		
		if(imc < 16.0) return "Você está clinicamente morto!";
		else if(imc <= 16.9) return "Muito abaixo do peso";
		else if(imc <= 18.4) return "Abaixo do peso";
		else if(imc <= 24.9) return "Peso normal";
		else if(imc <= 29.9) return "Acima do peso";
		else if(imc <= 34.9) return "Obesidade Grau I";
		else if(imc <= 40.0) return "Obesidade Grau II";
		else return "Obesidade Grau III";
	}
}