package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Classe base de registro de medições
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class RegistroMedicao {
	protected Date dataHoraRegistro;
	
	public RegistroMedicao() {
		super();
		this.dataHoraRegistro = new Date();		
	}	

	/**
	 * Data e hora de registro
	 */
	public Date getDataHoraRegistro() {
		return dataHoraRegistro;
	}
}
