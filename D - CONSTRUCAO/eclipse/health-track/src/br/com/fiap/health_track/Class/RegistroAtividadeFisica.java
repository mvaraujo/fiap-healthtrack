package br.com.fiap.health_track.Class;

/**
 * Registro de atividade física
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class RegistroAtividadeFisica extends RegistroMedicaoComentada {
	private ExercicioCategoria exercicioCategoria;
	private int calorias;

	/**
	 * @param exercicioCategoria
	 * @param calorias
	 * @param descricao
	 */
	public RegistroAtividadeFisica(ExercicioCategoria exercicioCategoria, int calorias, String descricao) {
		super(descricao);
		this.exercicioCategoria = exercicioCategoria;
		this.calorias = calorias;
	}

	/**
	 * Categoria de exercício
	 */
	public ExercicioCategoria getExercicioCategoria() {
		return exercicioCategoria;
	}

	/**
	 * Calorias queimadas com a atividade
	 */
	public int getCalorias() {
		return calorias;
	}
}