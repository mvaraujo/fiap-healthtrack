package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Registro de medição de peso
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class RegistroPeso extends RegistroMedicao{
	private double peso;

	/**
	 * @param peso
	 */
	public RegistroPeso(double peso) {
		super();
		this.peso = peso;
		this.dataHoraRegistro = new Date();
	}

	/**
	 * Peso registrado
	 */
	public double getPeso() {
		return peso;
	}
}