package br.com.fiap.health_track.Class;

/**
 * Classe base de registro de medições com comentário
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class RegistroMedicaoComentada extends RegistroMedicao {
	protected String descricao;
	
	public RegistroMedicaoComentada(String descricao) {
		super();
		this.descricao = descricao;		
	}	

	/**
	 * Anotações sobre a atividade
	 */
	public String getDescricao() {
		return descricao;
	}
}
