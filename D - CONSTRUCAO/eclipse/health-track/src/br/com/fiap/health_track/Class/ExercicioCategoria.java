package br.com.fiap.health_track.Class;

/**
 * Categoria de exercício
 */
public class ExercicioCategoria {
	private ExercicioCategoria categoriaPai;
	private String titulo;

	public ExercicioCategoria(ExercicioCategoria categoriaPai, String titulo) {
		super();
		this.categoriaPai = categoriaPai;
		this.titulo = titulo;
	}

	/**
	 * Se houver, categoria pai
	 */
	public ExercicioCategoria getCategoriaPai() {
		return categoriaPai;
	}

	public void setCategoriaPai(ExercicioCategoria categoriaPai) {
		this.categoriaPai = categoriaPai;
	}

	/**
	 * Título da categoria
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}