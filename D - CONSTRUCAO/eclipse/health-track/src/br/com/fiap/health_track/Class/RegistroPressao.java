package br.com.fiap.health_track.Class;

/**
 * Registro de medição de pressão
 * 
 * <b>Todas as propriedads são somente leitura</b>
 */
public class RegistroPressao extends RegistroMedicao {
	private int pressaoSistolica;
	private int pressaoDiastolica;

	public RegistroPressao(int pressaoSistolica, int pressaoDiastolica) {
		super();
		this.pressaoSistolica = pressaoSistolica;
		this.pressaoDiastolica = pressaoDiastolica;
	}

	/**
	 * Medição de pressão sistólica
	 */
	public int getPressaoSistolica() {
		return pressaoSistolica;
	}

	/**
	 * Medição de pressão diastólica
	 */
	public int getPressaoDiastolica() {
		return pressaoDiastolica;
	}
}