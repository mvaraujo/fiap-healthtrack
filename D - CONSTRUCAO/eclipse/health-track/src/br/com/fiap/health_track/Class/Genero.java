package br.com.fiap.health_track.Class;

/**
 * Tipos de gênero
 */
public enum Genero {
	MASCULINO("M", "Masculino"), FEMININO("F", "Feminino");
	
	private String chave;
	private String titulo;
	
	Genero(String chave, String titulo)
	{
		this.chave = chave;
		this.titulo = titulo;
	}

	public String getChave() {
		return chave;
	}
	
	public String getTitulo() {
		return titulo;
	}
}