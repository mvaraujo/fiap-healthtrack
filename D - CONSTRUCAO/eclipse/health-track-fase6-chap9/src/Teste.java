import br.com.fiap.health_track.Class.*;
import br.com.fiap.health_track.Class.dao.*;

import java.sql.SQLException;
import java.util.*;

public class Teste {
	public static void main(String[] args) throws Exception {
		Random random = new Random();		
		
		try {
			Usuario usuario = UsuarioDAO.buscarPorEMail("mvsabino@gmail.com");

			if (usuario == null) {
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.YEAR, 1972);
				cal.set(Calendar.MONTH, Calendar.JANUARY);
				cal.set(Calendar.DAY_OF_MONTH, 29);
				
				usuario = new Usuario("Marcus Vinicius", "mvsabino@gmail.com", cal.getTime(), "M", 182);
				
				UsuarioDAO.cadastrar(usuario);				
			}
			
			/*----------------------------------------------------------------[]
			 * listRegistroAlimentacao
			 []---------------------------------------------------------------*/
			
			ArrayList<RegistroAlimentacao> listRegistroAlimentacao = RegistroAlimentacaoDAO.listarTodosPorUsuario(usuario);
			
			if(listRegistroAlimentacao.isEmpty())
			{
				RegistroAlimentacaoDAO.cadastrar(new RegistroAlimentacao(usuario, 100, "Primeira descri��o"));
				RegistroAlimentacaoDAO.cadastrar(new RegistroAlimentacao(usuario, 1000, "Segunda descri��o"));
				RegistroAlimentacaoDAO.cadastrar(new RegistroAlimentacao(usuario, 300, "Terceira descri��o"));
				RegistroAlimentacaoDAO.cadastrar(new RegistroAlimentacao(usuario, 600, "Quarta descri��o"));
				RegistroAlimentacaoDAO.cadastrar(new RegistroAlimentacao(usuario, 50, "Quinta descri��o"));
				
				listRegistroAlimentacao = RegistroAlimentacaoDAO.listarTodosPorUsuario(usuario);				
			}
			
			for(RegistroAlimentacao registroAlimentacao : listRegistroAlimentacao)
			{
				System.out.println(
						String.format(
								"%d calorias registradas com descri��oo \"%s\"",
								registroAlimentacao.getCalorias(),
								registroAlimentacao.getDescricao()
							)
					);		
			}
			
			/*----------------------------------------------------------------[]
			 * listRegistroAtividadeFisica
			 []---------------------------------------------------------------*/
			
			ArrayList<ExercicioCategoria> exercicioCategorias = ExercicioCategoriaDAO.listarTodos();
			
			if(exercicioCategorias.isEmpty())
			{
				ExercicioCategoriaDAO.cadastrar(new ExercicioCategoria(null, "Ciclismo"));
				ExercicioCategoriaDAO.cadastrar(new ExercicioCategoria(null, "Nata��o"));
				ExercicioCategoriaDAO.cadastrar(new ExercicioCategoria(null, "Corrida"));
				ExercicioCategoriaDAO.cadastrar(new ExercicioCategoria(null, "Muscula��o"));
				ExercicioCategoriaDAO.cadastrar(new ExercicioCategoria(null, "Yoga"));
				
				exercicioCategorias = ExercicioCategoriaDAO.listarTodos();				
			}
			
			ArrayList<RegistroAtividadeFisica> listRegistroAtividadeFisica = RegistroAtividadeFisicaDAO.listarTodosPorUsuario(usuario);
			
			if(listRegistroAtividadeFisica.isEmpty())
			{
				RegistroAtividadeFisicaDAO.cadastrar(new RegistroAtividadeFisica(usuario,  exercicioCategorias.get(random.nextInt(5)), random.nextInt(1500) + 1, "Atividade 1"));
				RegistroAtividadeFisicaDAO.cadastrar(new RegistroAtividadeFisica(usuario,  exercicioCategorias.get(random.nextInt(5)), random.nextInt(1500) + 1, "Atividade 2"));
				RegistroAtividadeFisicaDAO.cadastrar(new RegistroAtividadeFisica(usuario,  exercicioCategorias.get(random.nextInt(5)), random.nextInt(1500) + 1, "Atividade 3"));
				RegistroAtividadeFisicaDAO.cadastrar(new RegistroAtividadeFisica(usuario,  exercicioCategorias.get(random.nextInt(5)), random.nextInt(1500) + 1, "Atividade 4"));
				RegistroAtividadeFisicaDAO.cadastrar(new RegistroAtividadeFisica(usuario,  exercicioCategorias.get(random.nextInt(5)), random.nextInt(1500) + 1, "Atividade 5"));
				
				listRegistroAtividadeFisica = RegistroAtividadeFisicaDAO.listarTodosPorUsuario(usuario);				
			}
			
			for(RegistroAtividadeFisica registroAtividadeFisica : listRegistroAtividadeFisica)
			{
				System.out.println(
						String.format(
								"%d calorias queimadas com %s com descri��oo \"%s\"",
								registroAtividadeFisica.getCalorias(),
								registroAtividadeFisica.getExercicioCategoria().getTitulo(),
								registroAtividadeFisica.getDescricao()
							)
					);		
			}			

			/*----------------------------------------------------------------[]
			 * listRegistroPeso
			 []---------------------------------------------------------------*/
			
			ArrayList<RegistroPeso> listRegistroPeso = RegistroPesoDAO.listarTodosPorUsuario(usuario);
			
			if(listRegistroPeso.isEmpty())
			{
				RegistroPesoDAO.cadastrar(new RegistroPeso(usuario, 95.5));
				RegistroPesoDAO.cadastrar(new RegistroPeso(usuario, 95.6));
				RegistroPesoDAO.cadastrar(new RegistroPeso(usuario, 97.0));
				RegistroPesoDAO.cadastrar(new RegistroPeso(usuario, 96.0));
				RegistroPesoDAO.cadastrar(new RegistroPeso(usuario, 96.0));
				
				listRegistroPeso = RegistroPesoDAO.listarTodosPorUsuario(usuario);				
			}
			
			for(RegistroPeso registroPeso : listRegistroPeso)
			{
				System.out.println(
						String.format(
								"Peso %f registrado em %tc",
								registroPeso.getPeso(),
								registroPeso.getDataHoraRegistro()
							)
					);		
			}
			
			/*----------------------------------------------------------------[]
			 * listRegistroPressao
			 []---------------------------------------------------------------*/
			
			ArrayList<RegistroPressao> listRegistroPressao = RegistroPressaoDAO.listarTodosPorUsuario(usuario);
			
			if(listRegistroPressao.isEmpty())
			{
				RegistroPressaoDAO.cadastrar(new RegistroPressao(usuario, 12, 8));
				RegistroPressaoDAO.cadastrar(new RegistroPressao(usuario, 13, 8));
				RegistroPressaoDAO.cadastrar(new RegistroPressao(usuario, 12, 9));
				RegistroPressaoDAO.cadastrar(new RegistroPressao(usuario, 13, 9));
				RegistroPressaoDAO.cadastrar(new RegistroPressao(usuario, 12, 8));
				
				listRegistroPressao = RegistroPressaoDAO.listarTodosPorUsuario(usuario);				
			}
			
			for(RegistroPressao registroPressao : listRegistroPressao)
			{
				System.out.println(
						String.format(
								"Sist %d X Diast %d registrada em %tc",
								registroPressao.getPressaoSistolica(),
								registroPressao.getPressaoDiastolica(),
								registroPressao.getDataHoraRegistro()
							)
					);		
			}
		} catch (SQLException e) {
			System.err.println("N�o foi poss�vel conectar no Banco de Dados");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("O Driver JDBC n�o foi encontrado!");
			e.printStackTrace();
		}
	}
}