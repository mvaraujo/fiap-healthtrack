package br.com.fiap.health_track.Class.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import br.com.fiap.health_track.Class.*;

/**
 * Registro de alimentação DAO
 */

public class RegistroAlimentacaoDAO {
	static public void cadastrar(RegistroAlimentacao registroAlimentacao) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("insert into UsuarioRegistroAlimentacao(Id, UsuarioId, Calorias, Descricao, DataHoraRegistro)\n");
			sql.append("values(S_USUARIO_REGISTRO_ALIMENTACAO.NEXTVAL, ?, ?, ?, ?)");

			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				stmt.setInt(1, registroAlimentacao.getUsuario().getId());
				stmt.setInt(2, registroAlimentacao.getCalorias());
				stmt.setString(3, registroAlimentacao.getDescricao());
				stmt.setDate(4, new java.sql.Date(registroAlimentacao.getDataHoraRegistro().getTime()));

				stmt.executeUpdate();
			}
		}
	}

	static public ArrayList<RegistroAlimentacao> listarTodosPorUsuario(Usuario usuario) throws SQLException, Exception {
		ArrayList<RegistroAlimentacao> list = new ArrayList<RegistroAlimentacao>();

		try (Connection conexao = DBManager.obterConexao()) {
			try (PreparedStatement stmt = conexao.prepareStatement("select * from UsuarioRegistroAlimentacao where UsuarioId = ?")) {
				stmt.setInt(1, usuario.getId());

				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						list.add(new RegistroAlimentacao(usuario, rs.getInt("Calorias"), rs.getDate("DataHoraRegistro"), rs.getString("Descricao")));
					}
				}
			}
		}

		return list;
	};
}