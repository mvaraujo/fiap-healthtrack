package br.com.fiap.health_track.Class.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import br.com.fiap.health_track.Class.*;

/**
 * Registro de Pesoo DAO
 */

public class RegistroPesoDAO {
	static public void cadastrar(RegistroPeso registroPeso) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("insert into UsuarioRegistrosPeso(Id, UsuarioId, Peso, DataHoraRegistro)\n");
			sql.append("values(S_USUARIO_REGISTRO_PESO.NEXTVAL, ?, ?, ?)");

			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				stmt.setInt(1, registroPeso.getUsuario().getId());
				stmt.setDouble(2, registroPeso.getPeso());
				stmt.setDate(3, new java.sql.Date(registroPeso.getDataHoraRegistro().getTime()));

				stmt.executeUpdate();
			}
		}
	}

	static public ArrayList<RegistroPeso> listarTodosPorUsuario(Usuario usuario) throws SQLException, Exception {
		ArrayList<RegistroPeso> list = new ArrayList<RegistroPeso>();

		try (Connection conexao = DBManager.obterConexao()) {
			try (PreparedStatement stmt = conexao.prepareStatement("select * from UsuarioRegistrosPeso where UsuarioId = ?")) {
				stmt.setInt(1, usuario.getId());

				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						list.add(new RegistroPeso(usuario, rs.getDouble("Peso"), rs.getDate("DataHoraRegistro")));
					}
				}
			}
		}

		return list;
	};
}