package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Registro de medi��o de press�o
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class RegistroPressao extends RegistroMedicao {
	private int pressaoSistolica;
	private int pressaoDiastolica;

	public RegistroPressao(Usuario usuario, int pressaoSistolica, int pressaoDiastolica) {
		super(usuario);
		this.pressaoSistolica = pressaoSistolica;
		this.pressaoDiastolica = pressaoDiastolica;
	}

	public RegistroPressao(Usuario usuario, int pressaoSistolica, int pressaoDiastolica, Date dataHoraRegistro) {
		super(usuario);
		this.pressaoSistolica = pressaoSistolica;
		this.pressaoDiastolica = pressaoDiastolica;
		this.dataHoraRegistro = dataHoraRegistro;
	}

	/**
	 * Medi��o de press�o sist�lica
	 */
	public int getPressaoSistolica() {
		return pressaoSistolica;
	}

	/**
	 * Medi��o de press�o diast�lica
	 */
	public int getPressaoDiastolica() {
		return pressaoDiastolica;
	}
}