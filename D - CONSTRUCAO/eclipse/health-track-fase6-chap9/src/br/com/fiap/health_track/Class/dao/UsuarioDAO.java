package br.com.fiap.health_track.Class.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import br.com.fiap.health_track.Class.*;

/**
 * Usu�rio do sistema, com seus dados pessoais e seu hist�rico
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class UsuarioDAO {
	static public void cadastrar(Usuario usuario) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("insert into Usuarios(Id, NomeCompleto, DataNascimento, Genero, Altura, EMail, SenhaHash, SenhaSalt)\n");
			sql.append("values(S_USUARIO.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)");

			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				stmt.setString(1, usuario.getNomeCompleto());
				stmt.setDate(2, new java.sql.Date(usuario.getDataNascimento().getTime()));
				stmt.setString(3, usuario.getGenero().getChave());
				stmt.setInt(4, usuario.getAltura());
				stmt.setString(5, usuario.getEMail());
				stmt.setString(6, usuario.getSenhaHash());
				stmt.setInt(7, usuario.getSenhaSalt());

				stmt.executeUpdate();
			}
		}
	}

	static public Usuario buscarPorEMail(String email) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			try (PreparedStatement stmt = conexao.prepareStatement("select * from Usuarios where EMail = ?")) {
				stmt.setString(1, email);

				try (ResultSet rs = stmt.executeQuery()) {
					return buscarPorResultSet(rs);
				}
			}
		}
	}

	static private Usuario buscarPorResultSet(ResultSet rs) throws SQLException {
		if (rs.next()) {
			return new Usuario(rs.getInt("Id"), rs.getString("NomeCompleto"), rs.getString("EMail"), rs.getDate("DataNascimento"), rs.getString("Genero"), rs.getInt("Altura"), rs.getString("SenhaHash"),
					rs.getInt("SenhaSalt"));
		} else {
			return null;
		}
	}
}