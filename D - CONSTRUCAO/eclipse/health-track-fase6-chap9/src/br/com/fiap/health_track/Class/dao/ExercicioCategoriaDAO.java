package br.com.fiap.health_track.Class.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;
import br.com.fiap.health_track.Class.*;

/**
 * ExercicioCategoria DAO
 */

public class ExercicioCategoriaDAO {
	static public void cadastrar(ExercicioCategoria exercicioCategoria) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("insert into ExercicioCategorias(Id, PaiId, Titulo)\n");
			sql.append("values(S_EXERCICIO_CATEGORIA.NEXTVAL, ?, ?)");

			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				if(exercicioCategoria.getCategoriaPai() != null)
				{
					stmt.setInt(1, exercicioCategoria.getCategoriaPai().getId());					
				}
				else
				{
					stmt.setNull(1, Types.INTEGER);					
				}
				
				stmt.setString(2, exercicioCategoria.getTitulo());
				
				stmt.executeUpdate();
			}
		}
	}

	static public ArrayList<ExercicioCategoria> listarTodos() throws SQLException, Exception {
		ArrayList<ExercicioCategoria> list = new ArrayList<ExercicioCategoria>();

		try (Connection conexao = DBManager.obterConexao()) {
			try (PreparedStatement stmt = conexao.prepareStatement("select * from ExercicioCategorias")) {
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						list.add(new ExercicioCategoria(rs.getInt("Id"), null, rs.getString("Titulo")));
					}
				}
			}
		}

		return list;
	};
}