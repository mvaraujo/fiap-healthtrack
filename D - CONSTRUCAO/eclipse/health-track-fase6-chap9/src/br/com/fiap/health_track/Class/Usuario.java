package br.com.fiap.health_track.Class;

import java.util.Date;
import java.util.UUID;
import java.util.ArrayList;

/**
 * Usu�rio do sistema, com seus dados pessoais e seu hist�rico
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class Usuario {
	private int Id;
	private String nomeCompleto;
	private Date dataNascimento;
	private Genero genero;
	private int altura;

	private String eMail;
	private String senhaHash;
	private int senhaSalt;

	private ArrayList<RegistroPeso> registrosPeso;
	private ArrayList<RegistroPressao> registrosPressao;
	private ArrayList<RegistroAtividadeFisica> registrosAtividadeFisica;
	private ArrayList<RegistroAlimentacao> registrosAlimentacao;

	/**
	 * Construtor privado para garantir inicializa��o dos arrays
	 */
	private Usuario() {
		super();
		this.registrosPeso = new ArrayList<RegistroPeso>();
		this.registrosPressao = new ArrayList<RegistroPressao>();
		this.registrosAtividadeFisica = new ArrayList<RegistroAtividadeFisica>();
		this.registrosAlimentacao = new ArrayList<RegistroAlimentacao>();
	}

	/**
	 * @param nomeCompleto
	 * @param altura
	 */
	public Usuario(String nomeCompleto, String eMail, Date dataNascimento, String generoChave, int altura) {
		this();
		this.nomeCompleto = nomeCompleto;
		this.eMail = eMail;
		this.dataNascimento = dataNascimento;
		this.genero = Genero.buscarPorChave(generoChave);
		this.altura = altura;
		this.senhaHash = UUID.randomUUID().toString();
		this.senhaSalt = 0;
	}

	/**
	 * @param fromDB
	 * @param altura
	 */
	public Usuario(int id, String nomeCompleto, String eMail, Date dataNascimento, String generoChave, int altura, String senhaHash, int senhaSalt) {
		this();
		this.Id = id;		
		this.nomeCompleto = nomeCompleto;
		this.eMail = eMail;
		this.genero = Genero.buscarPorChave(generoChave);
		this.dataNascimento = dataNascimento;
		this.altura = altura;
		this.senhaHash = senhaHash;
		this.senhaSalt = senhaSalt;
	}

	/**
	 * Id
	 */
	public int getId() {
		return Id;
	}

	/**
	 * Nome completo
	 */
	public String getNomeCompleto() {
		return nomeCompleto;
	}

	/**
	 * Data de nascimento
	 */
	public Date getDataNascimento() {
		return dataNascimento;
	}

	/**
	 * Classifica��o de g�nero
	 */
	public Genero getGenero() {
		return genero;
	}

	/**
	 * Altura em metros
	 */
	public int getAltura() {
		return altura;
	}

	/**
	 * Endere�o de e-mail
	 */
	public String getEMail() {
		return eMail;
	}

	/**
	 * Hash da senha de acesso
	 */
	public String getSenhaHash() {
		return senhaHash;
	}

	/**
	 * Salt do hash da senha de acesso
	 */
	public int getSenhaSalt() {
		return senhaSalt;
	}

	/**
	 * Registra medi��o de peso
	 */
	public void registraPeso(double peso) {
		this.registrosPeso.add(new RegistroPeso(this, peso));
	}

	/**
	 * Registra medi��o de press�o
	 */
	public void registraPressao(int pressaoSistolica, int pressaoDiastolica) {
		this.registrosPressao.add(new RegistroPressao(this, pressaoSistolica, pressaoDiastolica));
	}

	/**
	 * Registra pr�tica de atividade f�sica
	 */
	public void registraAtividadeFisica(ExercicioCategoria exercicioCategoria, int calorias, String anotacoes) {
		this.registrosAtividadeFisica.add(new RegistroAtividadeFisica(this, exercicioCategoria, calorias, anotacoes));
	}

	/**
	 * Registra alimenta��o
	 */
	public void registraAlimentacao(int calorias, String descricao) {
		this.registrosAlimentacao.add(new RegistroAlimentacao(this, calorias, descricao));
	}

	/**
	 * Calcula o valor do IMC de acordo com a �ltima medi��o de pesos
	 * 
	 * @throws Exception caso n�o haja medi��es de peso
	 */
	public double imcValor() throws Exception {
		if (registrosPeso.isEmpty()) {
			throw new Exception("[imcValor] N�o h� pesos registrados!");
		}

		return registrosPeso.get(registrosPeso.size() - 1).getPeso() / Math.pow(this.altura, 2);
	}

	/**
	 * Retorna a descri��o para imcValor()
	 */
	public String imcDescricao() throws Exception {
		double imc = this.imcValor();

		if (imc < 16.0)
			return "Voc� est� clinicamente morto!";
		else if (imc <= 16.9)
			return "Muito abaixo do peso";
		else if (imc <= 18.4)
			return "Abaixo do peso";
		else if (imc <= 24.9)
			return "Peso normal";
		else if (imc <= 29.9)
			return "Acima do peso";
		else if (imc <= 34.9)
			return "Obesidade Grau I";
		else if (imc <= 40.0)
			return "Obesidade Grau II";
		else
			return "Obesidade Grau III";
	}
}