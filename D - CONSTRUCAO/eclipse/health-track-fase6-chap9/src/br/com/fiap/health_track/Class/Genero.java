package br.com.fiap.health_track.Class;

import java.util.NoSuchElementException;

/**
 * Tipos de g�nero
 */

public enum Genero {
	MASCULINO("M", "Masculino"), FEMININO("F", "Feminino");

	private String chave;
	private String titulo;

	Genero(String chave, String titulo) {
		this.chave = chave;
		this.titulo = titulo;
	}

	public String getChave() {
		return chave;
	}

	public String getTitulo() {
		return titulo;
	}

	public static Genero buscarPorChave(String chave) {
		for (Genero g : values()) {
			if (g.chave.equals(chave)) {
				return g;
			}
		}
		
		throw new NoSuchElementException(String.format("[Genero.buscarPorChave] N�o encontrado '%s'", chave));
	}
}