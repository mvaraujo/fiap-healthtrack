package br.com.fiap.health_track.Class.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import br.com.fiap.health_track.Class.*;

/**
 * Registro de Pesoo DAO
 */

public class RegistroPressaoDAO {
	static public void cadastrar(RegistroPressao registroPressao) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("insert into UsuarioRegistrosPressao(Id, UsuarioId, PressaoSistolica, PressaoDiastolica, DataHoraRegistro)\n");
			sql.append("values(S_USUARIO_REGISTRO_PRESSAO.NEXTVAL, ?, ?, ?, ?)");

			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				stmt.setInt(1, registroPressao.getUsuario().getId());
				stmt.setInt(2, registroPressao.getPressaoSistolica());
				stmt.setInt(3, registroPressao.getPressaoDiastolica());
				stmt.setDate(4, new java.sql.Date(registroPressao.getDataHoraRegistro().getTime()));

				stmt.executeUpdate();
			}
		}
	}

	static public ArrayList<RegistroPressao> listarTodosPorUsuario(Usuario usuario) throws SQLException, Exception {
		ArrayList<RegistroPressao> list = new ArrayList<RegistroPressao>();

		try (Connection conexao = DBManager.obterConexao()) {
			try (PreparedStatement stmt = conexao.prepareStatement("select * from UsuarioRegistrosPressao where UsuarioId = ?")) {
				stmt.setInt(1, usuario.getId());

				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						list.add(new RegistroPressao(usuario, rs.getInt("PressaoSistolica"), rs.getInt("PressaoDiastolica"), rs.getDate("DataHoraRegistro")));
					}
				}
			}
		}

		return list;
	};
}