package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Registro de medi��o de peso
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class RegistroPeso extends RegistroMedicao{
	private double peso;

	/**
	 * @param peso
	 */
	public RegistroPeso(Usuario usuario, double peso) {
		super(usuario);
		this.peso = peso;
		this.dataHoraRegistro = new Date();
	}

	/**
	 * @param peso
	 * @para dataHoraRegistro
	 */
	public RegistroPeso(Usuario usuario, double peso, Date dataHoraRegistro) {
		super(usuario);
		this.peso = peso;
		this.dataHoraRegistro = dataHoraRegistro;
	}

	/**
	 * Peso registrado
	 */
	public double getPeso() {
		return peso;
	}
}