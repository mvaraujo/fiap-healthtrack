package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Classe base de registro de medi��es
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class RegistroMedicao {
	protected Usuario usuario;
	protected Date dataHoraRegistro;
	
	public RegistroMedicao(Usuario usuario) {
		super();
		this.usuario = usuario;
		this.dataHoraRegistro = new Date();		
	}
	
	/***
	 * Usu�rio do registro
	 */	
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * Data e hora de registro
	 */
	public Date getDataHoraRegistro() {
		return dataHoraRegistro;
	}
}
