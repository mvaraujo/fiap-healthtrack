package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Registro de atividade f�sica
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class RegistroAtividadeFisica extends RegistroMedicaoComentada {
	private ExercicioCategoria exercicioCategoria;
	private int calorias;

	/**
	 * @param exercicioCategoria
	 * @param calorias
	 * @param descricao
	 */
	public RegistroAtividadeFisica(Usuario usuario, ExercicioCategoria exercicioCategoria, int calorias, String descricao) {
		super(usuario, descricao);
		this.exercicioCategoria = exercicioCategoria;
		this.calorias = calorias;
		this.descricao = descricao;
	}

	/**
	 * @param exercicioCategoria
	 * @param calorias
	 * @param dataHoraRegistro
	 * @param descricao
	 */
	public RegistroAtividadeFisica(Usuario usuario, ExercicioCategoria exercicioCategoria, int calorias, Date dataHoraRegistro, String descricao) {
		super(usuario, descricao);
		this.exercicioCategoria = exercicioCategoria;
		this.calorias = calorias;
		this.dataHoraRegistro = dataHoraRegistro;
		this.descricao = descricao;
	}

	/**
	 * Categoria de exerc�cio
	 */
	public ExercicioCategoria getExercicioCategoria() {
		return exercicioCategoria;
	}

	/**
	 * Calorias queimadas com a atividade
	 */
	public int getCalorias() {
		return calorias;
	}
}