package br.com.fiap.health_track.Class;

/**
 * Categoria de exerc�cio
 */
public class ExercicioCategoria {
	private int id;
	private ExercicioCategoria categoriaPai;
	private String titulo;

	public ExercicioCategoria(ExercicioCategoria categoriaPai, String titulo) {
		super();
		this.categoriaPai = categoriaPai;
		this.titulo = titulo;
	}

	public ExercicioCategoria(int id, ExercicioCategoria categoriaPai, String titulo) {
		super();
		this.id = id;
		this.categoriaPai = categoriaPai;
		this.titulo = titulo;
	}

	/**
	 * Se houver, categoria pai
	 */
	public ExercicioCategoria getCategoriaPai() {
		return categoriaPai;
	}

	public void setCategoriaPai(ExercicioCategoria categoriaPai) {
		this.categoriaPai = categoriaPai;
	}

	/**
	 * ID da categoria
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * T�tulo da categoria
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}