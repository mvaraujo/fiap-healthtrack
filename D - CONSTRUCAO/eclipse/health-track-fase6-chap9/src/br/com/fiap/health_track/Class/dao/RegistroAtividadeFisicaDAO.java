package br.com.fiap.health_track.Class.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import br.com.fiap.health_track.Class.*;

/**
 * Registro de atividade f�sica DAO
 */

public class RegistroAtividadeFisicaDAO {
	static public void cadastrar(RegistroAtividadeFisica registroAtividadeFisica) throws Exception {
		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("insert into UsuarioRegistroAtividadeFisica(Id, UsuarioId, CategoriaId, Calorias, Anotacoes, DataHoraRegistro)\n");
			sql.append("values(S_USUARIO_REGISTRO_ATV_FISICA.NEXTVAL, ?, ?, ?, ?, ?)");

			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				stmt.setInt(1, registroAtividadeFisica.getUsuario().getId());
				stmt.setInt(2, registroAtividadeFisica.getExercicioCategoria().getId());
				stmt.setInt(3, registroAtividadeFisica.getCalorias());
				stmt.setString(4, registroAtividadeFisica.getDescricao());
				stmt.setDate(5, new java.sql.Date(registroAtividadeFisica.getDataHoraRegistro().getTime()));

				stmt.executeUpdate();
			}
		}
	}
	
	static public ArrayList<RegistroAtividadeFisica> listarTodosPorUsuario(Usuario usuario) throws SQLException, Exception {
		ArrayList<RegistroAtividadeFisica> list = new ArrayList<RegistroAtividadeFisica>();

		try (Connection conexao = DBManager.obterConexao()) {
			StringBuffer sql = new StringBuffer();

			sql.append("select uraf.*, ec.Titulo as CategoriaTitulo\n"); 
			sql.append("from\n"); 
			sql.append("    UsuarioRegistroAtividadeFisica uraf\n"); 
			sql.append("        inner join ExercicioCategorias ec\n"); 
			sql.append("        on ec.Id = uraf.CategoriaId\n");
			sql.append("where uraf.UsuarioId = ?\n");
			
			try (PreparedStatement stmt = conexao.prepareStatement(sql.toString())) {
				stmt.setInt(1, usuario.getId());

				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						ExercicioCategoria exercicioCategoria = new ExercicioCategoria(rs.getInt("CategoriaId"), null, rs.getString("CategoriaTitulo"));
						
						list.add(new RegistroAtividadeFisica(usuario, exercicioCategoria, rs.getInt("Calorias"), rs.getDate("DataHoraRegistro"), rs.getString("Anotacoes")));
					}
				}
			}
		}

		return list;
	};	
}