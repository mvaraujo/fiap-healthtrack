package br.com.fiap.health_track.Class;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Token gerado para reset de senha de um usu�rio
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class PaswdResetToken {
	private Usuario usuario;
	private UUID guid;
	private Date dataHoraExpiracao;

	/**
	 * @param <b>usuario</b> refer�ncia ao usu�rio
	 * @param <b>validadeHoras</b> quantidade de horas durante as quais o token ser�
	 *        v�lido
	 */
	public PaswdResetToken(Usuario usuario, int validadeHoras) {
		super();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, validadeHoras);

		this.usuario = usuario;
		this.guid = UUID.randomUUID();
		this.dataHoraExpiracao = cal.getTime();
	}

	/**
	 * Instância do usu�rio
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * GUID gerado
	 */
	public UUID getGuid() {
		return guid;
	}

	/**
	 * Data e hora em que o token expira
	 */
	public Date getDataHoraExpiracao() {
		return dataHoraExpiracao;
	}
}