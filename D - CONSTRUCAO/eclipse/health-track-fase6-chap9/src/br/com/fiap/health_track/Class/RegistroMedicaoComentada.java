package br.com.fiap.health_track.Class;

/**
 * Classe base de registro de medi��es com coment�rio
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class RegistroMedicaoComentada extends RegistroMedicao {
	protected String descricao;
	
	public RegistroMedicaoComentada(Usuario usuario, String descricao) {
		super(usuario);
		this.descricao = descricao;		
	}	

	/**
	 * Anota��es sobre a atividade
	 */
	public String getDescricao() {
		return descricao;
	}
}
