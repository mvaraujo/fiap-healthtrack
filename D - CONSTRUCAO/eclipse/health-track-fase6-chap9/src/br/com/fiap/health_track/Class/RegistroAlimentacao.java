package br.com.fiap.health_track.Class;

import java.util.Date;

/**
 * Registro de alimenta��o
 * 
 * <b>Todas as propriedads s�o somente leitura</b>
 */
public class RegistroAlimentacao extends RegistroMedicaoComentada {
	private int calorias;

	/**
	 * @param calorias
	 * @param descricao
	 */
	public RegistroAlimentacao(Usuario usuario, int calorias, String descricao) {
		super(usuario, descricao);
		this.calorias = calorias;
	}

	/**
	 * @param calorias
	 * @param dataHoraRegistro
	 * @param descricao
	 */
	public RegistroAlimentacao(Usuario usuario, int calorias, Date dataHoraRegistro, String descricao) {
		super(usuario, descricao);
		this.calorias = calorias;
	}

	/**
	 * Calorias ingeridas
	 */
	public int getCalorias() {
		return calorias;
	}
}